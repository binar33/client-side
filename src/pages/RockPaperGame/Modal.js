import React from "react";
import Modals from "../../components/Modal";

const Modal = () => {
  return (
    <>
      <Modals />
    </>
  );
};

export default Modal;

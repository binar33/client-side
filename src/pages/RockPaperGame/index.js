import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { Button } from "react-bootstrap";

import "../../assets/style/rock-paper.css";
import Logo2 from "../../assets/img/logo-2.png";
import Batu from "../../assets/img/batu.png";
import Kertas from "../../assets/img/kertas.png";
import Gunting from "../../assets/img/gunting.png";
import Refresh from "../../assets/img/refresh.png";
import { ReactComponent as LifeIcon } from "../../assets/icons/loveIcon.svg";
import { getRandomNumberInt } from "./GetRandomInt";
import Loading from "../../components/Loading";
import Modals from "../../components/Modal";

const RockPaperGame = () => {
  const [isLoding, setIsLoding] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [playerChoose, setPlayerChoose] = useState("");
  const [comChoose, setComChoose] = useState("");
  const [resultGame, setResultGame] = useState("");
  const [isPlayerChoose, setIsPlayerChoose] = useState(false);
  const [counterClick, setCounterClick] = useState(true);
  const [buttonRefresh, setButtonRefresh] = useState(false);
  const [life, setLife] = useState(3);
  const [startScore, setStartScore] = useState(0);
  const [higherScore, setHigherScore] = useState(null);
  const [reFetchData, setreFetchData] = useState(true);
  const [backgroundColorComBatu, setBackgroundColorComBatu] =
    useState("transparent");
  const [backgroundColorComKertas, setBackgroundColorComKertas] =
    useState("transparent");
  const [backgroundColorComGunting, setBackgroundColorComGunting] =
    useState("transparent");

  useEffect(() => {
    const fetch = async () => {
      try {
        const dataScore = await axios.get(
          "https://restfull-api-fsw24-binar.herokuapp.com/score-user",
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setHigherScore(dataScore.data.data.score);
        setIsLoding(false);
      } catch (error) {
        alert(error);
        setIsLoding(false);
      }
    };

    if (reFetchData) {
      fetch();
      setreFetchData(false);
    }
  }, [reFetchData, setreFetchData]);

  useEffect(() => {
    if (resultGame === "You win") {
      setStartScore(startScore + 10);
    }
    if (resultGame === "You Loser") {
      if (life > 1) {
        setLife(life - 1);
      } else {
        setShowModal(true);
        setLife(life - 1);
        if (startScore > higherScore) {
          setIsLoding(true);
          try {
            const postData = async () => {
              const newScore = await axios.post(
                "https://restfull-api-fsw24-binar.herokuapp.com/score-user",
                { score: startScore },
                {
                  headers: { Authorization: `${Cookies.get("token")}` },
                }
              );
              setHigherScore(newScore.data.score);
            };
            postData();
            setreFetchData(true);
          } catch (error) {
            alert(error);
            setIsLoding(false);
          }
        }
      }
    }
  }, [resultGame, setResultGame]);

  const rollGame = (player, com) => {
    if (player === com) return `draw`;
    else if (player === `batu`) {
      if (com === `gunting`) return `You win`;
      else return `You Loser`;
    } else if (player === `kertas`) {
      if (com === `batu`) return `You win`;
      else return `You Loser`;
    } else if ((player = `gunting`)) {
      if (com === `kertas`) return `You win`;
      else return `You Loser`;
    }
  };

  const drawingColorCom = () => {
    let randomNumber = getRandomNumberInt(1, 3);
    let randomColor = `rgb(${getRandomNumberInt(0, 255)},${getRandomNumberInt(
      0,
      255
    )},${getRandomNumberInt(0, 255)})`;
    if (randomNumber === 1) {
      setBackgroundColorComBatu(randomColor);
      setBackgroundColorComKertas("transparent");
      setBackgroundColorComGunting("transparent");
    } else if (randomNumber === 2) {
      setBackgroundColorComBatu("transparent");
      setBackgroundColorComKertas(randomColor);
      setBackgroundColorComGunting("transparent");
    } else {
      setBackgroundColorComBatu("transparent");
      setBackgroundColorComKertas("transparent");
      setBackgroundColorComGunting(randomColor);
    }
  };

  const resetColor = () => {
    setBackgroundColorComBatu("transparent");
    setBackgroundColorComKertas("transparent");
    setBackgroundColorComGunting("transparent");
  };

  const comSelect = (com) => {
    if (com === 0) return "batu";
    else if (com === 1) return "kertas";
    else return "gunting";
  };

  const handlerPlayerChoose = (player) => {
    if (counterClick) {
      setCounterClick(false);
      let interval;
      setPlayerChoose(player);
      setIsPlayerChoose(true);
      const com = getRandomNumberInt(0, 2);
      if (!interval) interval = setInterval(drawingColorCom, 50);

      setTimeout(() => {
        clearInterval(interval);
        let comResult = comSelect(com);
        setComChoose(comResult);
        resetColor();
        setResultGame(rollGame(player, comResult));
        setButtonRefresh(true);
      }, 2000);

      setTimeout(() => {
        setCounterClick(true);
        setPlayerChoose("");
        setComChoose("");
        setIsPlayerChoose(false);
        setResultGame("");
        setButtonRefresh(false);
      }, 4000);
    }
  };

  const handleRefresh = () => {
    setCounterClick(true);
    setPlayerChoose("");
    setComChoose("");
    setIsPlayerChoose(false);
    setResultGame("");
    setButtonRefresh(false);
  };

  const handlerButtonModal = () => {
    setShowModal(false);
    setStartScore(0);
    setLife(3);
    setCounterClick(true);
    setPlayerChoose("");
    setComChoose("");
    setIsPlayerChoose(false);
    setResultGame("");
    setButtonRefresh(false);
  };

  const lifeComponents = () => {
    const result = [];
    for (let i = 0; i < life; i++) {
      result.push(
        <div key={i} id="icon-life-game" className="w-20 h-20 pt-1 ms-1">
          <LifeIcon width={`30px`} />
        </div>
      );
    }
    return result;
  };

  const BodyModal = () => {
    return (
      <div className="text-center h4">
        <p>Score permainan anda : {startScore}</p>
        <p>
          Score terbaik anda :{" "}
          {higherScore > startScore ? higherScore : startScore}
        </p>
      </div>
    );
  };

  return (
    <div className="container-game">
      {isLoding && <Loading />}
      <Modals
        title={Cookies.get("user")}
        body={<BodyModal />}
        footer={<Button onClick={handlerButtonModal}>Oke</Button>}
        handleClose={handlerButtonModal}
        show={showModal}
      />
      <section className="container-fluid pt-lg-2 position-relative">
        {/* HEADER */}
        <div className="header row align-items-center">
          <div className="col-1 ps-lg-5 me-5 me-lg-0">
            <button>
              <Link to="/home">&lt; </Link>
            </button>
          </div>
          <div className="col-1">
            <img src={Logo2} alt="" />
          </div>
          <div className="col-lg row ps-lg-0 mx-auto mx-lg-0 pb-5 pb-lg-0">
            <strong> ROCK PAPER SCISSORS</strong>
          </div>
        </div>
        {/* END HEADER */}
        {/* CONTENT */}
        <div
          id="container-text-name-game"
          className="d-flex justify-content-between pe-5"
        >
          <div id="life-name-game" className="ps-5 pt-5 d-flex gap-2">
            <p className="h3 text-white">Life :</p>
            {life ? <>{lifeComponents().map((el) => el)}</> : null}
          </div>
          <div id="text-name-game" className="d-flex flex-column text-white h2">
            <div>{Cookies.get("user")}</div>
            <div>Score tertinggi anda: {higherScore ? higherScore : ""}</div>
          </div>
        </div>
        <div id="score-permainan" className="ps-5">
          <p className="h3 text-white">Score permainan: {startScore}</p>
        </div>
        <div className="content-game row pt-lg-3 justify-content-evenly justify-content-lg-center">
          {/* PLAYER 1 */}
          <div className="col-4 col-lg-3 position-relative">
            <h3 className="row mb-1 mb-lg-5 fs-1 justify-content-center fw-bold mx-auto mx-lg-0">
              You
            </h3>
            <div
              onClick={() => handlerPlayerChoose("batu")}
              className={`${playerChoose === "batu" ? "player-choose" : ""} ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player row content mb-1 mb-lg-5 justify-content-center mx-auto`}
              data-player="batu"
            >
              <img className="batu align-self-center" src={Batu} alt="" />
            </div>
            <div
              onClick={() => handlerPlayerChoose("kertas")}
              className={`${playerChoose === "kertas" ? "player-choose" : ""} ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player row content mb-1 mb-lg-5 justify-content-center mx-auto`}
              data-player="kertas"
            >
              <img className="align-self-center" src={Kertas} alt="" />
            </div>
            <div
              onClick={() => handlerPlayerChoose("gunting")}
              className={`${
                playerChoose === "gunting" ? "player-choose" : ""
              } ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player row content mb-1 mb-lg-5 justify-content-center mx-auto`}
              data-player="gunting"
            >
              <img className="align-self-center" src={Gunting} alt="" />
            </div>
          </div>
          {/* END PLAYER 1 */}
          <div className="col-2 align-self-center text-center">
            {resultGame === "" ? (
              <div className="vs row text-center">
                <strong className="col d-flex justify-content-center">
                  VS
                </strong>
              </div>
            ) : (
              <div className="result row text-center">
                <strong className="text-result col">{resultGame}</strong>
              </div>
            )}
          </div>
          {/* COM */}
          <div className="col-4 col-lg-3">
            <h3 className="row mb-1 mb-lg-5 fs-1 justify-content-center fw-bold mx-auto mx-lg-0">
              COM
            </h3>
            <div
              style={{ backgroundColor: backgroundColorComBatu }}
              className={`com-batu ${
                comChoose === "batu" ? "player-choose" : ""
              } row content mb-1 mb-lg-5 justify-content-center mx-auto`}
            >
              <img className="batu align-self-center" src={Batu} alt="" />
            </div>
            <div
              style={{ backgroundColor: backgroundColorComKertas }}
              className={`com-kertas ${
                comChoose === "kertas" ? "player-choose" : ""
              } row content mb-1 mb-lg-5 justify-content-center mx-auto`}
            >
              <img className="align-self-center" src={Kertas} alt="" />
            </div>
            <div
              style={{ backgroundColor: backgroundColorComGunting }}
              className={`com-gunting ${
                comChoose === "gunting" ? "player-choose" : ""
              } row content mb-1 mb-lg-5 justify-content-center mx-auto`}
            >
              <img className="align-self-center" src={Gunting} alt="" />
            </div>
          </div>
          {/* END COM */}
        </div>
        {/* END CONTENT */}
        {/* REFRESH */}
        {buttonRefresh && (
          <button onClick={handleRefresh} className="refresh position-absolute">
            <img src={Refresh} alt="" />
          </button>
        )}
        {/* END REFRESH */}
      </section>
    </div>
  );
};

export default RockPaperGame;

import axios from "axios";
import Cookies from "js-cookie";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import LogoLogin from "../../assets/img/logo-2.png";
import "../../assets/style/login.css";
import Loading from "../../components/Loading";
import { DataContext } from "../../stateManagement";

const Login = () => {
  const [textHeader, setTextHeader] = useState("Log in");
  const [isWrongInput, setIsWrongInput] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [input, setInput] = useState({
    username: "",
    password: "",
  });
  const { back, setBack } = useContext(DataContext);

  const navigate = useNavigate();

  const handlerChangeInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmitLogin = async (e) => {
    e.preventDefault();
    setisLoading(true);
    try {
      const user = await axios.post(
        "https://restfull-api-fsw24-binar.herokuapp.com/login",
        input
      );
      Cookies.set("user", user.data.name);
      Cookies.set("token", user.data.accessToken);
      navigate("/home");
      setInput({
        username: "",
        password: "",
      });
      setisLoading(false);
    } catch (error) {
      //   console.log(error.response.status);
      if (error.response.status === 402) {
        setTextHeader("Username Incorrect!");
        setIsWrongInput(true);
        setisLoading(false);
      }
      if (error.response.status === 401) {
        setTextHeader("Password Incorrect!");
        setIsWrongInput(true);
        setisLoading(false);
      }
    }
  };

  const params = useLocation().pathname;

  useEffect(() => {
    setBack(params);
  }, []);

  return (
    <div className="wrapper">
      {isLoading && <Loading />}
      <div className="logo">
        <img src={LogoLogin} alt="logo" />
      </div>
      <div
        className={`text-center mt-4 name ${isWrongInput ? "text-danger" : ""}`}
      >
        {textHeader}
      </div>
      <form
        method="POST"
        action="POST"
        onSubmit={handlerSubmitLogin}
        className="p-3 mt-3"
      >
        <div className="form-field d-flex align-items-center">
          <span className="far fa-user" />
          <input
            type="text"
            name="username"
            value={input.username}
            onChange={handlerChangeInput}
            id="userName"
            placeholder="Username"
          />
        </div>
        <div className="form-field d-flex align-items-center">
          <span className="fas fa-key" />
          <input
            type="password"
            name="password"
            value={input.password}
            onChange={handlerChangeInput}
            id="pwd"
            placeholder="Password"
          />
        </div>
        <button type="submit" className="btn mt-3">
          Login
        </button>
      </form>
      <div className="text-center fs-6">
        <Link to="#">Forget password?</Link> or
        <Link to="/registrasi">Sign up</Link>
      </div>
    </div>
  );
};

export default Login;

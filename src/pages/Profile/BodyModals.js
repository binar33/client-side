const BodyModals = ({ input, setInput }) => {
  const handleChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div>
      <div>
        <p className="m-0">Name:</p>
        <input
          className="form-control mb-3"
          name="name"
          type="text"
          value={input.name}
          onChange={handleChange}
        />
      </div>
      <div>
        <p className="m-0">Username:</p>
        <input
          className="form-control mb-3"
          name="username"
          type="text"
          value={input.username}
          onChange={handleChange}
        />
      </div>
      <div>
        <p className="m-0">Email:</p>
        <input
          className="form-control mb-3"
          name="email"
          type="email"
          value={input.email}
          onChange={handleChange}
        />
      </div>
      <div>
        <p className="m-0">Address:</p>
        <input
          className="form-control mb-3"
          name="address"
          type="text"
          value={input.address}
          onChange={handleChange}
        />
      </div>
      <div>
        <p className="m-0">Gender:</p>
        <select
        className=" mb-3"
          defaultValue={input.gender}
          onChange={handleChange}
          required
          name="gender"
        >
          <option value={"Male"}>Male</option>
          <option value={"Female"}>Female</option>
        </select>
      </div>
      <div>
        <p className="m-0">Social Media:</p>
        <input
          className="form-control mb-3"
          name="social_media_url"
          type="text"
          value={input.social_media_url}
          onChange={handleChange}
        />
      </div>
    </div>
  );
};

export default BodyModals;

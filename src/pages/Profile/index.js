import React, { useEffect, useState } from "react";
import "../../assets/style/profile.css";
import axios from "axios";
import Cookies from "js-cookie";
import Loading from "../../components/Loading";
import Modals from "../../components/Modal";
import BodyModals from "./BodyModals";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const Profile = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState(null);
  const [fetch, setFetch] = useState(true);
  const [openModals, setOpenModals] = useState(false);
  const [input, setInput] = useState({
    username: "",
    name: "",
    email: "",
    gender: "",
    address: "",
    social_media_url: "",
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const newUser = await axios.get(
          "https://restfull-api-fsw24-binar.herokuapp.com/user",
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setInput({
          username: newUser.data.data.username,
          email: newUser.data.data.email,
          name: newUser.data.data.name,
          gender: newUser.data.data.gender,
          address: newUser.data.data.address,
          social_media_url: newUser.data.data.social_media_url,
        });
        setIsLoading(false);
        setUser(newUser.data.data);
      } catch (error) {
        setIsLoading(false);
        alert(error);
      }
    };
    if (fetch) {
      fetchData();
      setFetch(false);
    }
  }, [fetch, setFetch]);

  const handlerEdit = async () => {
    setIsLoading(true);
    try {
      const userUpdate = await axios.post(
        "https://restfull-api-fsw24-binar.herokuapp.com/update-user",
        input,
        {
          headers: { Authorization: Cookies.get("token") },
        }
      );
      setFetch(false);
      setIsLoading(false);
      setUser(userUpdate.data.data);
      setOpenModals(false);
    } catch (error) {
      setIsLoading(false);
      if (error.response.status === 402) {
        alert("Username already exist!");
      }
      if (error.response.status === 401) {
        alert("email already exist!");
      }
      alert(error)
    }
  };

  const handlerCloseModals = () => {
    setOpenModals(false);
    setInput(user);
  };

  return (
    <div>
      {isLoading && <Loading />}
      {
        <Modals
          title="Edit Profile"
          show={openModals}
          handleClose={handlerCloseModals}
          body={<BodyModals input={input} setInput={setInput} />}
          footer={
            <button
              className="btn btn-primary button-register"
              onClick={handlerEdit}
            >
              Save
            </button>
          }
        />
      }
      <Card className="container">
        <div className="text-center mt-5">
          <h1 className="m-0"> {user?.name} </h1>
          <p className="fs-3  mb-4">{user?.username}</p>
        </div>
        <div className="line-profile  fs-5 ">
          <p>
            Email:<span className="ms-4">{user?.email}</span>
          </p>
          <p>
            Address:<span className="ms-2">{user?.address}</span>
          </p>
          <p>
            Gender:
            <span className="ms-3">{user?.gender}</span>
          </p>
          <p>
            Social Media:
            <span className="ms-2">{user?.social_media_url}</span>
          </p>
        </div>
        <div className="d-flex justify-content-end">
          <Button
            onClick={() => setOpenModals(true)}
            variant="primary button-register mb-3 me-5"
          >
            Edit
          </Button>
        </div>
      </Card>
    </div>
  );
};

export default Profile;

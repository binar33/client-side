import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";
import Loading from "../../components/Loading";

const divStyl = {
  width: "18rem",
  backgroundSize: "cover",
};

const GameList = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const getData = async () => {
    const { data } = await axios.get(`https://restfull-api-fsw24-binar.herokuapp.com/gamelist`);
    setData(data.data);
    setIsLoading(false);
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <div>
      <Container fluid="md">
      {isLoading && <Loading />}
        <Row xs="auto">
          {data.map((game) => (
            <Col key={game.id}>
              <Card style={divStyl} className="mb-3">
                <Card.Body>
                  <Card.Title>{game.name}</Card.Title>
                  <Card.Text>
                    <br></br>
                  </Card.Text>
                  <Link to={`detail/${game.name}`} className="mx-0">
                    Detail Game
                  </Link>
                  {/* <Link to="/Game/:name" className="mx-3">
                    Play Game
                  </Link> */}
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
};

export default GameList;

import React, { useState, useContext } from "react";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import "../../assets/style/regis.css";
import Loading from "../../components/Loading";
import { DataContext } from "../../stateManagement";
import Button from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

const Regis = () => {
  const [input, setInput] = useState({
    username: "",
    name: "",
    email: "",
    gender: "",
    address: "",
    social_media_url: "",
    password: "",
  });
  const { back, setBack } = useContext(DataContext);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();
  const handlerChangeInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  const handlerSubmitRegis = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const user = await axios.post(
        "https://restfull-api-fsw24-binar.herokuapp.com/registrasi",
        input
      );
      setIsLoading(false);
      navigate("/login");
      setInput({
        username: "",
        name: "",
        email: "",
        gender: "",
        address: "",
        social_media_url: "",
        password: "",
      });
    } catch (error) {
      setIsLoading(false);
      if (error.response.status === 402) {
        alert("Username already exist!");
      }
      if (error.response.status === 401) {
        alert("email already exist!");
      }
    }
  };

  return (
    <div className="wrapper-register">
      {isLoading && <Loading />}
      <form method="post" action="post" onSubmit={handlerSubmitRegis}>
        <div className="text-center fw-bold fs-3">Register</div>
        <div className="row mt-5 ">
          <div className=" col-lg-6 mb-3">
            <label htmlFor="name">Name</label>
            <input
              required
              id="name"
              type="text"
              className="form-control"
              value={input.name}
              onChange={handlerChangeInput}
              name="name"
              placeholder="fullname"
            />
          </div>
          <div className="col-lg-6 mb-3">
            <label htmlFor="username">Username</label>
            <input
              required
              id="username"
              type="text"
              className="form-control"
              value={input.username}
              onChange={handlerChangeInput}
              name="username"
              placeholder="username (1 word)"
            />
          </div>
        </div>
        <div className="row ">
          <div className="col-lg-6 mb-3">
            <label htmlFor="email">Email</label>
            <input
              required
              id="email"
              type="email"
              className="form-control"
              value={input.email}
              onChange={handlerChangeInput}
              name="email"
              placeholder="name@example.com"
            />
          </div>
          <div className="col-lg-6 mb-3">
            <label htmlFor="password">Password</label>
            <input
              required
              id="password"
              type="password"
              className="form-control"
              value={input.password}
              onChange={handlerChangeInput}
              name="password"
              placeholder="password"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 mb-3">
            <label htmlFor="address">Address</label>
            <input
              required
              id="address"
              type="text"
              className="form-control"
              value={input.address}
              onChange={handlerChangeInput}
              name="address"
              placeholder="address"
            />
          </div>
          <div className="col-lg-6 mb-3">
            <select
              onChange={handlerChangeInput}
              className="mt-4 select-register"
              required
              name="gender"
            >
              <option value={""}>Gender</option>
              <option value={"Male"}>Male</option>
              <option value={"Female"}>Female</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div>
            <label htmlFor="social_media_url">Social Media</label>
            <input
              required
              id="social_media_url"
              type="text"
              className="form-control"
              value={input.social_media_url}
              onChange={handlerChangeInput}
              name="social_media_url"
              placeholder="social_media_url"
            />
          </div>
        </div>
        <div className="d-flex justify-content-end">
          <Link to={back}>
            <button className="btn btn-danger button-register me-2 mt-5">
              Cancel
            </button>
          </Link>
          {input.username === "" ||
          input.gender === "" ||
          input.name === "" ||
          input.email === "" ||
          input.password === "" ||
          input.social_media_url === "" ? (
            <OverlayTrigger
              overlay={<Tooltip>form tidak boleh kosong!</Tooltip>}
            >
              <Button
                disabled
                variant=" btn mt-5 me-5 button-register  bg-secondary me-2 text-white"
              >
                Register
              </Button>
            </OverlayTrigger>
          ) : (
            <Button
              type="submit"
              variant="btn mt-5 me-5 button-register text-white bg-primary"
            >
              Register
            </Button>
          )}
        </div>
      </form>
    </div>
  );
};

export default Regis;

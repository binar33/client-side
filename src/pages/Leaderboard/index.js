import React, { useEffect, useState } from "react";
import axios from "axios";
import Loading from "../../components/Loading";
import { ReactComponent as PialaIcon } from "../../assets/icons/pialaIcon.svg";
import "../../assets/style/leaderboard.css";

const Leaderboard = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [fetchData, setFetchData] = useState(true);
  const [data, setData] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        const dataLeaderboard = await axios.get(
          "https://restfull-api-fsw24-binar.herokuapp.com/leaderboard"
        );
        setIsLoading(false);
        setData(dataLeaderboard.data.data);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fetchData) fetch();
    setFetchData(false);
  }, [fetchData, setFetchData]);

  const List = ({ number, className, name, score }) => {
    return (
      <div
        id="container-list-leaderboard"
        className={`d-flex position-relative justify-content-evenly w-100 shadow bg-slate p-3 rounded-4 mb-4 ${className}`}
      >
        <p
          id="number-leaderboard"
          className="position-absolute start-0 fw-bold ps-3 rounded"
        >
          {number}
        </p>
        <p id="name-leaderboard" className="h4 px-3 py-1 rounded-3 shadow">
          {name}
        </p>
        <p id="score-leaderboard" className="fw-bold">
          Highest Score: {score}
        </p>
      </div>
    );
  };

  const handleTextname = (params) => {
    if (params.length > 10) {
      let result = "";
      for (let i = 0; i < 9; i++) {
        result += params[i];
      }
      result += "...";
      return result;
    }

    return params;
  };

  return (
    <div
      id="container-leaderboard"
      className="d-flex justify-content-between overflow-auto max-h-500px p-5"
    >
      {isLoading && <Loading />}
      <div
        id="content-leaderboard"
        className="col-7 bg-slate overflow-auto custom-scroll-bar rounded-3 p-3"
      >
        {data !== null && (
          <>
            {data.map((el, index) => {
              let background = "";
              if (index === 0) background = "bg-lightblue";
              if (index === 1) background = "bg-warning";
              return (
                <List
                  key={index}
                  number={index + 1}
                  name={handleTextname(el.name)}
                  score={el.Score.score}
                  className={background}
                />
              );
            })}
          </>
        )}
      </div>
      <div id="congrats" className="col-3 text-center">
        <h2 className="mb-5 text-primary fw-bold">🥳CONGRATSSS🥳</h2>
        {data !== null && (
          <>
            <p className="h3 text-danger fw-bold text-shadow">
              🎉{data[0].name}🎉
            </p>
          </>
        )}
        <p id="logo-piala" className="text-danger">
          <PialaIcon />
        </p>
      </div>
    </div>
  );
};

export default Leaderboard;

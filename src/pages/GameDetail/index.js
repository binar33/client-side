import React from "react";
import { useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import Loading from "../../components/Loading";

const GameDetail = () => {
  const { name } = useParams();
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const getData = async () => {
    const { data } = await axios.get(
      `https://restfull-api-fsw24-binar.herokuapp.com/game-list/detail/${name}`
    );
    setData(data.data);
    setIsLoading(false);
  };
  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Container fluid="md">
      {isLoading && <Loading />}
      <Row>
        <Col>
          <h5 style={{ textDecoration: "underline" }}>Nama Game</h5>
          <h5>{data.name}</h5>
          <br></br>
          <h5 style={{ textDecoration: "underline" }}>Diskripsi Game</h5>
          <h5>{data.description}</h5>
          <br></br>
          <h5 style={{ textDecoration: "underline" }}>Dibuat Pada Tanggal</h5>
          <h5>{data.createdAt}</h5>
          <br></br>
        </Col>
      </Row>
    </Container>
  );
};

export default GameDetail;

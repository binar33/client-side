import React from "react";
import Cookies from "js-cookie";
import {
  Routes,
  Route,
  BrowserRouter,
  Navigate,
  HashRouter,
} from "react-router-dom";
import DashboardLayout from "../layouts/Dashboard";
import GameList from "../pages/GameList";
import Home from "../pages/Home";
import Landing from "../pages/Landing";
import Login from "../pages/Login";
import Profile from "../pages/Profile";
import GameDetail from "../pages/GameDetail"
import StateManagement from "../stateManagement";
import Regis from "../pages/Regis";
import RockPaperGame from "../pages/RockPaperGame";
import Leaderboard from "../pages/Leaderboard";

const Router = () => {
  const PrivateRouteDashboard = ({ ...props }) => {
    if (Cookies.get("user")) return <DashboardLayout {...props} />;
    else return <Navigate to="/" />;
  };
  const PrivateRouteGame = ({ content }) => {
    if (Cookies.get("user")) return content;
    else return <Navigate to="/" />;
  };

  const HandlerLogin = ({ content }) => {
    if (Cookies.get("user")) return <Navigate to="/" />;
    else return content;
  };

  const HandlerRegister = ({ content }) => {
    if (Cookies.get("user")) return <Navigate to="/" />;
    else return content;
  };

  return (
    <HashRouter>
      <StateManagement>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route
            path="/registrasi"
            element={<HandlerRegister content={<Regis />} />}
          />
          <Route path="/login" element={<HandlerLogin content={<Login />} />} />
          <Route
            path="/home"
            element={<PrivateRouteDashboard content={<Home />} />}
          />
          <Route
            path="/profile"
            element={<PrivateRouteDashboard content={<Profile />} />}
          />
          <Route
            path="/game-list"
            element={<PrivateRouteDashboard content={<GameList />} />}
          />
          <Route
            path="/leaderboard"
            element={<PrivateRouteDashboard content={<Leaderboard />} />}
          />
          <Route
            path="/rock-paper-game"
            element={<PrivateRouteGame content={<RockPaperGame />} />}
          />
          <Route
            path="/game-list/detail/:name"
            element={<PrivateRouteDashboard content={<GameDetail />} />}
          />
        </Routes>
      </StateManagement>
    </HashRouter>
  );
};

export default Router;

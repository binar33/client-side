import React, { useContext } from "react";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import { DataContext } from "../../stateManagement";
import "../../assets/style/layoutDashboard.css";
import Navbar from "../../components/Navbar";

const DashboardLayout = (props) => {
  const { sideBarCollapse,tes,setTes } = useContext(DataContext);
  return (
    <div id="container-dashboard" className={`d-flex bg-slate minvh`}>
      <div
        id="sidebar-dahsboard"
        className={`${
          sideBarCollapse ? "w-5" : "w-20"
        } bg-white rounded shadow pt-5 duration-700`}
      >
        <Sidebar />
      </div>
      <div id="navbar-dashboard">
        <div>
          <Navbar />
        </div>
      </div>
      <div id="container-content-dashboard" className="col bg-slate p-4 pb-5">
        <div
          id="content-dashboard"
          className="bg-white h-100 rounded-4 shadow-lg pt-3"
        >
          <div className="w-75 border border-secondary p-3 mx-auto rounded-5 shadow mb-3">
            <Header />
          </div>
          <div>{props.content}</div>
        </div>
        <div
          id="footer-dashboard"
          className="d-flex justify-content-between p-1 grey-light"
        >
          <p>24 October 2022 | Challenge Chapter 9 Binar | FSW 24</p>
          <p>
            Created by Tim 1 : <i>Ricky Ramadani</i>, <i>Kurniawan</i>,{" "}
            <i>Naufal</i>
          </p>
        </div>
      </div>
    </div>
  );
};

export default DashboardLayout;

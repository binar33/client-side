import React from "react";
import Spinner from "react-bootstrap/Spinner";

const Loading = () => {
  return (
    <div className="position-absolute top-0 bottom-0 start-0 end-0 bg-slate z-index-30 opacity-50 d-flex justify-content-center">
      <div className="align-self-center">
        <Spinner className="z-index-35" animation="border" variant="primary" />
        <Spinner
          className="z-index-35"
          animation="border"
          variant="secondary"
        />
        <Spinner className="z-index-35" animation="border" variant="success" />
        <Spinner className="z-index-35" animation="border" variant="danger" />
        <Spinner className="z-index-35" animation="border" variant="warning" />
        <Spinner className="z-index-35" animation="border" variant="info" />
        <Spinner className="z-index-35" animation="border" variant="dark" />
      </div>
    </div>
  );
};

export default Loading;

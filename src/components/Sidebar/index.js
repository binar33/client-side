import React from "react";
import { dataListMenuSidebar } from "./ListSidebar";

import { Link, useLocation } from "react-router-dom";
import { ReactComponent as ButtonSidebarIcon } from "../../assets/icons/leftIcon.svg";
import { useContext } from "react";
import { DataContext } from "../../stateManagement";

const Sidebar = (props) => {
  const { sideBarCollapse, setSideBarCollapse,setTes,tes } = useContext(DataContext);
  const pathName = useLocation().pathname;

  const splitterPathname = pathName.split("/");

  // const SidebarLogo = (props) => {
  //   return (
  //     <div className="flex gap-2 justify-center">
  //       <div className="flex items-stretch">
  //         <img className="w-9 h-9 self-end" src={props.logo} alt="logo" />
  //       </div>
  //       <div className="flex items-stretch">
  //         <h2
  //           className={`${
  //             sideBarCollapse ? "hidden" : ""
  //           } self-center text-4xl font-extrabold font-mono text-yellow-600`}
  //         >
  //           RestoKita
  //         </h2>
  //       </div>
  //     </div>
  //   );
  // };

  const SidebarMenu = (props) => {
    return (
      <div className={`${sideBarCollapse ? "pl-1" : "pl-10"}`}>
        {props.children}
      </div>
    );
  };

  const ItemSidebar = (props) => {
    const { className, label, link, icon } = props;

    return (
      <li
        className={` ${
          sideBarCollapse ? "pl-1" : "pl-6"
        } text-decoration-none list-group-item p-1 px-2 mb-2 ${className}`}
      >
        <Link
          className="flex text-decoration-none text-warning text-hover fw-semibold items-stretch gap-0"
          to={link}
        >
          <span className="flex self-center text-black">
            {icon}&nbsp;&nbsp;
          </span>
          {!sideBarCollapse && <span>{label}</span>}
        </Link>
      </li>
    );
  };

  const SidebarListMenu = (props) => {
    const itemMenuSidebar = props.data.map((item, index) => {
      let locationStyle = "";

      if (splitterPathname[1] === item.link.split("/")[1])
        locationStyle = "border-start border-2 border-warning";
      return (
        <ItemSidebar
          key={index}
          icon={item.icon}
          label={item.name}
          link={item.link}
          className={locationStyle}
        />
      );
    });

    return itemMenuSidebar;
  };

  const SidebarBtnCollapse = () => {
    return (
      <button
        className={`${
          sideBarCollapse ? "rotate-180" : ""
        } position-absolute bg-black duration-700 text-warning border border-2 border-warning rounded-circle pb-1 px-2 end-0 top-0`}
        onClick={() => {
          setSideBarCollapse(!sideBarCollapse);
          setTes(!tes)
        }}
      >
        <ButtonSidebarIcon width="12px" />
      </button>
    );
  };

  return (
    <div className="ps-4 position-relative">
      <div className="pt-5">
        {/* <SidebarLogo logo={logo} /> */}
        <SidebarMenu>
          <SidebarListMenu data={dataListMenuSidebar} />
        </SidebarMenu>
        <SidebarBtnCollapse />
      </div>
    </div>
  );
};

export default Sidebar;

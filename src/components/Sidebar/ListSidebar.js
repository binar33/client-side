import { ReactComponent as DashboardIcon } from "../../assets/icons/dashboardIcon.svg";
import { ReactComponent as GameIcon } from "../../assets/icons/gameIcon.svg";
import { ReactComponent as ProfileIcon } from "../../assets/icons/profileIcon.svg";
import { ReactComponent as PialaIcon } from "../../assets/icons/pialaIcon.svg";

const dataListMenuSidebar = [
  {
    label: "home",
    name: "Home",
    link: "/home",
    icon: <DashboardIcon width="17px" />,
  },
  {
    label: "profile",
    name: "Profile",
    link: "/profile",
    icon: <ProfileIcon width="17px" />,
  },
  {
    label: "game-list",
    name: "Game List",
    link: "/game-list",
    icon: <GameIcon width="17px" />,
  },
  {
    label: "leaderboard",
    name: "Leaderboard",
    link: "/leaderboard",
    icon: <PialaIcon width="17px" />,
  },
];

export { dataListMenuSidebar };

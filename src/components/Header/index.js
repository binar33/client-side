import Cookies from "js-cookie";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Dropdown from "react-bootstrap/Dropdown";
import { ReactComponent as BackIcon } from "../../assets/icons/backIcon.svg";
import "../../assets/style/header.css";

const Header = () => {
  const navigate = useNavigate();
  const handlerLogout = () => {
    Cookies.remove("user");
    Cookies.remove("token");
    navigate("/");
  };

  return (
    <div id="container-header" className="d-flex justify-content-between">
      <div
        id="container-text-header"
        className="d-flex gap-5 justify-content-between"
      >
        <Link
          className="text-decoration-none text-warning hover-warning fw-bold"
          to="/"
        >
          <BackIcon id="back-icon-header" width={"35px"} />
        </Link>
        <Link
          className="play-game text-decoration-none bg-warning px-2 py-1 rounded shadow-lg text-white hover-text-black fw-bold"
          to="/rock-paper-game"
        >
          Play Game
        </Link>
      </div>
      <div>
        <div>
          <Dropdown>
            <Dropdown.Toggle variant="warning" id="dropdown-header">
              {Cookies.get("user")}
            </Dropdown.Toggle>

            <Dropdown.Menu id="dropdown-menu-header">
              <Dropdown.Item className="text-warning">
                {Cookies.get("user")}
              </Dropdown.Item>
              <Dropdown.Item onClick={handlerLogout}>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};

export default Header;

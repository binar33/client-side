import { ReactComponent as DashboardIcon } from "../../assets/icons/dashboardIcon.svg";
import { ReactComponent as GameIcon } from "../../assets/icons/gameIcon.svg";
import { ReactComponent as ProfileIcon } from "../../assets/icons/profileIcon.svg";
import { ReactComponent as PialaIcon } from "../../assets/icons/pialaIcon.svg";

const dataListMenuNavbar = [
  {
    label: "home",
    link: "/home",
    icon: <DashboardIcon width="17px" />,
  },
  {
    label: "profile",
    link: "/profile",
    icon: <ProfileIcon width="17px" />,
  },
  {
    label: "game-list",
    link: "/game-list",
    icon: <GameIcon width="17px" />,
  },
  {
    label: "leaderboard",
    link: "/leaderboard",
    icon: <PialaIcon width="17px" />,
  },
];

export { dataListMenuNavbar };

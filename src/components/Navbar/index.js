import React from "react";
import { Link, useLocation } from "react-router-dom";
import { dataListMenuNavbar } from "./DataListNavbar";

const Navbar = () => {
  const pathName = useLocation().pathname;
  const splitterPathname = pathName.split("/");
  const ItemNavbar = ({ icon, label, link }) => {
    let styleLocation = "";
    if (splitterPathname[1] === link.split("/")[1])
      styleLocation = "border-bottom border-warning";
    return (
      <Link
        className={`text-warning hover-orange text-decoration-none ${styleLocation}`}
        to={link}
      >
        {icon}
        {label}
      </Link>
    );
  };

  const NavbarMenu = ({ data }) => {
    const result = data.map((el, index) => {
      return (
        <ItemNavbar
          key={index}
          icon={el.icon}
          label={el.label}
          link={el.link}
        />
      );
    });

    return result;
  };

  return (
    <div className="px-2 pt-2 position-fixed start-0 end-0">
      <div className="d-flex justify-content-evenly mx-auto p-2 rounded-5 shadow bg-white">
        <NavbarMenu data={dataListMenuNavbar} />
      </div>
    </div>
  );
};

export default Navbar;

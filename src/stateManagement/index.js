import { createContext, useState } from "react";

export const DataContext = createContext();

const StateManagement = (props) => {
  const [sideBarCollapse, setSideBarCollapse] = useState(false);
  const [back, setBack] = useState("");
  return (
    <DataContext.Provider
      value={{
        sideBarCollapse,
        setSideBarCollapse,
        back,
        setBack
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default StateManagement;
